
<a name="oxygen-add-on"></a>
# Installation als oXygen Add-On

Anwender*innen, die das SUB Schema Framework im oXygen XML-Editor in vollem Umfang nutzen wollen, können es als [oXygen Add-On](https://www.oxygenxml.com/doc/versions/26.0/ug-editor/glossary/plugin.html) installieren. Hierbei handelt es sich um die einfachste Art das SUB Schema Framework zu installieren. Außerdem kümmert sich oXygen bei dieser Installationart in der Folge selbständig um Updates, sprich: Wenn eine neue Version veröffentlicht wird, weist oXygen darauf hin und das Framework kann direkt auf die neueste Version geupdated werden!

Um das SUB Schema Framework als Add-On zu installieren, wird wie folgt verfahren:



- Kopiere diesen Link: [`https://gitlab.gwdg.de/teip5-sub-schema/releases/-/raw/main/updates.xml`](https://gitlab.gwdg.de/teip5-sub-schema/releases/-/raw/main/updates.xml). Das ist der Link zur entityXML Update Site, die alle veröffentlichten Versionen von entityXML listet.
- Klicke in oXygen auf `Hilfe > Neue Add-Ons installieren`.
- Kopierten Link in dem Feld `Add-Ons zeigen von` einfügen.
- Nun kann die aktuelle (oder eine alte) Version von SUB Schema installiert werden.
- Abschließend weist oXygen darauf hin, dass die Anwendung neu gestartet werden muss.


Nun sind sie im Besitz von SUB Schema als oXygen Framework und können es ohne Einschränkungen nutzen!