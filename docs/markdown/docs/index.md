
<a name="introduction"></a>
# SUB Schema Framework in a Nutshell

Das SUB Schema Framework ist ein oXygen Framework, dass parallel zum [**TEI-P5 Anwendungsprofil der Niedersächsischen Staats- und Universitätsbibliothek Göttingen**](https://gitlab.gwdg.de/teip5-sub-schema/tei-p5-sub-schema) (kurz: *SUB Schema*) entwickelt wird.


<a name="about"></a>
# Über dieses Handbuch

Dieses Handbuch ist als Handbuch für die Benutzung des TEI-P5 SUB Schema Frameworks gedacht. Es soll in erster Linie als Einführung und Nachschlagewerk in die Verwendung das Frameworks dienen, enthält aber auch technische Hinweise zur Verwendung hilfreicher Komponenten, die ebenfalls Teil des Frameworks sind.

Neben diesem Handbuch ist die [**Dokumentation des hinterliegenden TEI-P5 Anwendungsprofils**](http://teip5-sub-schema.pages.gwdg.de/tei-p5-sub-schema/p5sub_schema.html) für auszeichnungsspezifische Fragen zu konsultieren.

Dieses Handbuch befindet sich im Aufbau! Daher können Fehler zum Einen nicht ausgeschlossen werden. Zum Anderen sind viele Bereich bisher nur unvollständig oder garnicht dokumentiert. Das wird sich in der nächsten Zeit ändern.

Für Fragen, Anregungen etc. zu SUB Schema stehe ich gerne auch persönlich zur Verfügung: [sikora[at]sub.uni-goettingen.de](mailto:sikora@sub.uni-goettingen.de).
