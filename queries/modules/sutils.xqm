module namespace sutils = "http://sub.uni-goettingen.de/met/sub-schema/utils";
(:declare namespace output="w3.org/2010/xslt-xquery-serialization";:)
(:declare option output:method "xml"; :)

declare function sutils:replace-virgel( $strings as node()* ) {
    for $string in $strings
    let $replace := '<parsed>'||replace($string, '/', '<pc type="virgel" xmlns="http://www.tei-c.org/ns/1.0">/</pc>')||'</parsed>'
    return parse-xml( $replace )/parsed/node()
};

