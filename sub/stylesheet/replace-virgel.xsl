<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs" xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    version="2.0">
    
    <xsl:output indent="yes"></xsl:output>
    
    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="text()">
        <xsl:variable name="replace" select="'&lt;parsed&gt;'||replace(., '/', '&lt;pc type=&quot;virgel&quot;/&gt;')||'&lt;/parsed&gt;'"/>
        <xsl:copy-of select="parse-xml($replace)/*:parsed/node()"/>
    </xsl:template>
    
</xsl:stylesheet>