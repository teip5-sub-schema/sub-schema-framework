<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:oxy="http://www.oxygenxml.com/ns/author/xpath-extension-functions" exclude-result-prefixes="oxy">     
    
    <xsl:template match="/">
        <xsl:apply-templates select="oxy:current-element()"/>
    </xsl:template>
    
    <xsl:template match="*:uwe">
        ${caret}
        <BUH>
        <xsl:copy>
            <xsl:apply-templates/>
        </xsl:copy>
        </BUH>
    </xsl:template>

    <xsl:template match="a">
        <HALLO/>
    </xsl:template>
    
</xsl:stylesheet>