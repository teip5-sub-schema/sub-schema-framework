<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns="http://www.tei-c.org/ns/1.0" 
    xmlns:tei="http://www.tei-c.org/ns/1.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    version="2.0" 
    xmlns:oxy="http://www.oxygenxml.com/ns/author/xpath-extension-functions" 
    exclude-result-prefixes="oxy">     
    
    <xsl:template match="/">
        <xsl:apply-templates select="oxy:current-element()"/>
    </xsl:template>
    
    <xsl:template match="tei:bibl">
        <cit>
            ${caret}
            <xsl:apply-templates select="following::node()"/>
            <xsl:copy>
                <xsl:apply-templates/>
            </xsl:copy>
        </cit>
    </xsl:template>
    
    <xsl:template match="tei:quote">
        <xsl:copy>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>