---
Title: SUB-Schema oXygen Framework
Author: Uwe Sikora
DateCreated: 2023-10-27
---

# SUB-Schema oXygen Framework
Ein Framework für den oXygen XML Editor zur Bearbeitung von TEI-XML Daten nach Vorgaben des [TEI P5 SUB-Schemas](https://gitlab.gwdg.de/teip5-sub-schema/tei-p5-sub-schema).

Die Author-Mode Ansicht bringt zahlreiche Schaltflächen sowie einen detatiliert aufbereiteten CSS View auf TEI-XML Daten mit.

## Docs (GitLab Pages)

Die offizielle Dokumentation zum SUB SChema Framework findet sich unter http://teip5-sub-schema.pages.gwdg.de/sub-schema-framework.

## Setup
Choose a directory on your computer and clone the repository to it using git. 

``` bash
git clone git@gitlab.gwdg.de:teip5-sub-schema/sub-schema-framework.git
```

The repository is using [git-submodules](https://git-scm.com/book/de/v2/Git-Tools-Submodule) to integrate the above mentioned SUB-Schema Repository, so using `git` is the most conveniant way, but you need to keep in mind to initialise and update the submodule:

```bash
git submodule init
git submodule update
```

After cloning the repository and its submodules you may open oXygen and tell it how to use the Framework:

1) Go to `Optionen` > `Einstellungen`
2) Choose `Dokumenttypen-Zuordnung` from the List on the left and chose the subentry `Orte`
3) Add the path to the parent directory to `Zusätzliche Framework-Verzeichnisse` where your SUB-Schema Folder lives on your computer

For further Information on oXygen Frameworks you may visit oXygens Dokumentation on [Frameworks](https://www.oxygenxml.com/doc/versions/26.0/ug-editor/glossary/framework.html) in general, [Creating and Configuring Custom Frameworks](https://www.oxygenxml.com/doc/versions/26.0/ug-editor/topics/author-devel-guide-intro.html) and [Sharing a Framework](https://www.oxygenxml.com/doc/versions/26.0/ug-editor/topics/author-document-type-extension-sharing.html).